<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //Bio
    public function Bio(){
        return view('registrasi');
    }

    //Sign-Up
    public function Sign_Up(Request $request) {
        //dd($request->all());
        $first_name = $request['first_name'];
        $last_name = $request['last_name'];
        return view('welcome', compact('first_name','last_name'));
    }
}
