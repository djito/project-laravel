<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>    
    <h1>Buat Account baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last_name"><br><br>
        
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="1">Male <br>
        <input type="radio" name="gender" value="2">Female<br>
        <input type="radio" name="gender" value="3">Other<br><br>
        
        <label>Nationality:</label><br><br>
        <select id="cars" name="cars">
            <option value="wni">Indonesia</option>
            <option value="wna1">Singapura</option>
            <option value="wna2">Malaysia</option>
            <option value="wna3">Brunai</option>
            <option value="wna4">Other</option>
        </select><br><br>

        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="lang_s">Bahasa Indonesia<br>
        <input type="checkbox" name="lang_s">English<br>
        <input type="checkbox" name="lang_s">Other<br><br>
        
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>
        
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>